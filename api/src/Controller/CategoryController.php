<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Category;
use JMS\Serializer\SerializerInterface;
use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\JsonResponse;


class CategoryController extends AbstractController
{
    /**
     * @var Serializer
     */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/categories", methods="GET")
     */
    public function allCategories(CategoryRepository $categoryRepo)
    {
        $categories = $categoryRepo->findAll();
        $json = $this->serializer->serialize($categories, 'json');

        return new JsonResponse($json, 200, [], true);
    }

    /**
     * @Route("category/{category}", methods="GET")
     */
    public function oneCategory(Category $category)
    {
        return new JsonResponse($this->serializer->serialize($category, 'json'), 200, [], true);
    }
}
